class game {
    constructor(playerHand, cpuHand) {
        this.playerHand = playerHand
        this.cpuHand = cpuHand
    }

    rock(){
        playerRock.style.backgroundColor = "rgba(255,255,255, 0.8)"
        playerRock.style.borderRadius = "12px"
        this.CpuChoose()
        console.log("player adalah " + this.playerHand)
        console.log("cpu adalah " + this.cpuHand)
        this.playGame()
    }

    paper(){
        playerPaper.style.backgroundColor = "rgba(255,255,255, 0.8)"
        playerPaper.style.borderRadius = "12px"
        this.CpuChoose()
        console.log("player adalah " + this.playerHand)
        console.log("cpu adalah " + this.cpuHand)
        this.playGame()
    }
    
    scissors(){
        playerScissors.style.backgroundColor = "rgba(255,255,255, 0.8)"
        playerScissors.style.borderRadius = "12px"
        this.CpuChoose()
        console.log("player adalah " + this.playerHand)
        console.log("cpu adalah " + this.cpuHand)
        this.playGame()
    }

    CpuChoose(){
        let randomNumber = Math.floor(Math.random() * 3) + 1
        switch(randomNumber){
            case 1:
                this.cpuHand = "batu"
                cpuRock.style.backgroundColor = "rgba(255,255,255, 0.8)"
                cpuRock.style.borderRadius = "12px"
                break
            case 2:
                this.cpuHand = "kertas"
                cpuPaper.style.backgroundColor = "rgba(255,255,255, 0.8)"
                cpuPaper.style.borderRadius = "12px"
                break
            case 3:
                this.cpuHand = "gunting"
                cpuScissors.style.backgroundColor = "rgba(255,255,255, 0.8)"
                cpuScissors.style.borderRadius = "12px"
                break
        }
        
    }        

    playGame(){
        switch (this.playerHand){
            case this.cpuHand:
                indicator.textContent = "DRAW"
                indicator.style.backgroundColor = "#035B0C"
                indicator.style.color = "white"
                indicator.style.transform = 'rotate(-30deg)'
                indicator.style.fontSize = "1em"
                break
            case 'batu':
                switch(this.cpuHand){
                    case 'kertas':
                        indicator.textContent = "COM WINS"
                        indicator.style.transform = 'rotate(-30deg)'
                        indicator.style.backgroundColor = "#BD0000"
                        indicator.style.color = "white"
                        indicator.style.fontSize = "1em"
                        break
                    case 'gunting':
                        indicator.textContent = "PLAYER 1 WINS"
                        indicator.style.backgroundColor = "#4C9654"
                        indicator.style.color = "white"
                        indicator.style.transform = 'rotate(-30deg)'
                        indicator.style.fontSize = "1em"
                        break
                }
                break
            case 'kertas':
                switch(this.cpuHand){
                    case 'gunting':
                        indicator.textContent = "COM WINS"
                        indicator.style.transform = 'rotate(-30deg)'
                        indicator.style.backgroundColor = "#BD0000"
                        indicator.style.color = "white"
                        indicator.style.fontSize = "1em"
                        break
                    case 'batu':
                        indicator.textContent = "PLAYER 1 WINS"
                        indicator.style.backgroundColor = "#4C9654"
                        indicator.style.color = "white"
                        indicator.style.transform = 'rotate(-30deg)'
                        indicator.style.fontSize = "1em"
                        break
                }
                break
            case 'gunting':
                switch(this.cpuHand){
                    case 'batu':
                        indicator.textContent = "COM WINS"
                        indicator.style.transform = 'rotate(-30deg)'
                        indicator.style.backgroundColor = "#BD0000"
                        indicator.style.color = "white"
                        indicator.style.fontSize = "1em"
                        break
                    case 'kertas':
                        indicator.textContent = "PLAYER 1 WINS"
                        indicator.style.transform = 'rotate(-30deg)'
                        indicator.style.backgroundColor = "#4C9654"
                        indicator.style.color = "white"
                        indicator.style.fontSize = "1em"
                        break
                }
                break
        }
    
        // disable all buttons
        playerRock.disabled = true
        playerPaper.disabled = true
        playerScissors.disabled = true
    
    
    }

}

document.querySelector("#refresh").addEventListener('click', event => {
    window.location.reload();
});

const playerRock = document.getElementById('player-rock')
const playerPaper = document.getElementById('player-paper')
const playerScissors = document.getElementById('player-scissors')

const cpuRock = document.getElementById('cpu-rock')
const cpuPaper = document.getElementById('cpu-paper')
const cpuScissors = document.getElementById('cpu-scissors')

let indicator = document.getElementById('indicator')

rock = new game("batu")
paper = new game("kertas")
scissors = new game("gunting")